//
//  ViewController.swift
//  Apuntes
//
//  Created by Esteban Preciado on 3/5/19.
//  Copyright © 2019 Esteban Preciado. All rights reserved.
//

import UIKit
import PDFKit
import SafariServices


class ViewController: UIViewController, PDFViewDelegate {
    
    let pdfView = PDFView()
    let textView = UITextView()

    override func viewDidLoad() {
        super.viewDidLoad()
        pdfView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(pdfView)
        
        //Permite configurar el pdf view
        pdfView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        pdfView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        pdfView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        pdfView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        //Configuracion de TextView
        textView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(textView)
        textView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        textView.trailingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        textView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        textView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        textView.isEditable = false // No permite que se pueda escribir
        textView.isHidden = true // De inicio se esconda
        textView.textContainerInset = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        //Metodos de uso de pdf
        let search = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(promptForSearch))
        let share = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareSelection))
        let previous = UIBarButtonItem(barButtonSystemItem: .rewind, target: self.pdfView, action: #selector(pdfView.goToPreviousPage(_:)))
        let forward = UIBarButtonItem(barButtonSystemItem: .fastForward, target: self.pdfView, action: #selector(pdfView.goToNextPage(_:)))
        
        //Agrego a la barra de navegacion
        self.navigationItem.leftBarButtonItems = [search, previous, forward, share]
        self.pdfView.autoScales = true // Pantallas pequenas
        self.pdfView.delegate = self
        
        let pdfMode = UISegmentedControl(items: ["PDF", "TEXTO"])
        pdfMode.addTarget(self, action: #selector(changePdfMode), for: .valueChanged)
        pdfMode.selectedSegmentIndex = 0
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: pdfMode) // Agrego el boton a la derecha de la barra
        self.navigationItem.rightBarButtonItem?.width = 160
    }
    
    func load(_ name: String){
        //convertir el nombre del archivo al del fichero
        let fileName = name.replacingOccurrences(of: " ", with: "_").lowercased()
        //Busca dentro del paquete de recursos clase bundle, el archivo con extension pdf
        guard let path = Bundle.main.url(forResource: fileName, withExtension: "pdf") else{
          return
        }
        //Cargar pdf usando la clase pdf document con una URL
        if let document = PDFDocument(url: path){
            //Asignar el pdf document al pdf view de la app
            self.pdfView.document = document
            //Llamar al metodo gotofirstpage() para ir a la primera pagina.
            self.pdfView.goToFirstPage(nil)
            self.readText()
            //Mostrar el nombre del fichero en la barra de titulo del Ipad
            if UIDevice.current.userInterfaceIdiom == .pad {
                title = name
            }
        }
    }
    
    //Esta funcion agrega componentes para buscar texto dentro del pdf
    //Else return es un if else acortado
    @objc func promptForSearch(){
        let alert =  UIAlertController(title: "Buscar", message: nil, preferredStyle: .alert)
        alert.addTextField()
        
        //Boton Buscar
        alert.addAction(UIAlertAction(title: "Buscar", style: .default, handler: {action in
            guard let searchedText = alert.textFields?[0].text else {return}
            guard let match = self.pdfView.document?.findString(searchedText, fromSelection: self.pdfView.highlightedSelections?.first, withOptions: .caseInsensitive) else {return}
            self.pdfView.go(to: match) // Ira donde haga un "match"
            self.pdfView.highlightedSelections = [match]
        }))
        
        //Boton Cancelar
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        present(alert, animated: true)
    }
    
    @objc func shareSelection(sender: UIBarButtonItem){
        //Si existe alguna seleccion
        guard let selection = pdfView.currentSelection?.attributedString else {
            let alert = UIAlertController(title: "No existe nada seleccionado", message: "Seleccione una parte del documento para compartir", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alert, animated: true)
            return
        }
        
        let activityVC = UIActivityViewController(activityItems: [selection], applicationActivities: nil)
            activityVC.popoverPresentationController?.barButtonItem = sender
        present(activityVC, animated: true)
    }
    
    @objc func changePdfMode(segmentedControl: UISegmentedControl){
        if segmentedControl.selectedSegmentIndex == 0 {
            pdfView.isHidden = false
            textView.isHidden = true
        }else{
            pdfView.isHidden = true
            textView.isHidden = false
        }
    }
    
    func pdfViewWillClick(onLink sender: PDFView, with url: URL) {
        let viewController = SFSafariViewController(url: url)
        viewController.modalPresentationStyle = .formSheet
        present(viewController, animated: true)
    }
    
    /*Recorre todo el pdf, extrae el texto y lo agrega en una TextVIew*/
    func readText() {
        guard let pageCount = self.pdfView.document?.pageCount else {return}
        let pdfContent = NSMutableAttributedString()
        let space = NSAttributedString(string: "\n\n\n") //Agrega espacios para saber el final de la pagina
        
        for i in 1..<pageCount { //Pagina empieza en 1
            guard let page = self.pdfView.document?.page(at: i) else {continue}
            guard let pageContent = page.attributedString else {continue}
            pdfContent.append(space)
            pdfContent.append(pageContent)
        }
        
        let pattern = "https://frogames.es [0-9a-z].[a-z]"
        let regexp = try? NSRegularExpression(pattern: pattern)
        let range = NSMakeRange(0, pdfContent.string.utf16.count)
        
        if let matches = regexp?.matches(in: pdfContent.string, options: [], range: range){
            for match in matches.reversed(){
                pdfContent.replaceCharacters(in: match.range, with: "")
            }
        }
        
        self.textView.attributedText = pdfContent
    }
}

